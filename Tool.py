#!/usr/bin/env python3

import os

def StartsWithN(s):
	try:
		int(s[0])
		return True
	except ValueError:
		return False

def StripNonPics(Ls):
	Dirs = []
	for i in Ls:
		if StartsWithN(i) and os.path.isdir('Input/{}'.format(i)):
			Dirs += [i]
	return Dirs

def FormatCrds(InCrds):
	Crds = []
	for c in InCrds:
		i = c.split(';')
		Crds += [i]
		#if i[0] not in Crds:
	return Crds

def GetInputCrds():
	Ls = []
	try:
		Ls = os.listdir('Input')
	except Exception:
		print("Can't load Input pictures folder. Exiting.")
		exit(1)
	Ls = StripNonPics(Ls)
	Crds = FormatCrds(Ls)
	Crds.sort()
	return Crds # Format: X;Y;H

def FindNrst(Crd, Crds):
	RCrds = Crds
	RCrds.reverse()
	Nrst = []
	for i in range(0,4):
		curX = i[0]
		curY = i[1]
		if len(i) > 2:
			curH = i[2]
		else:
			curH = 0
		
		neaX, neaY, neaH, neaH_ = 0
		for c in Crds:
			if i == 0 and neaY == 0: # N
				curY = c[1] if c[1] > curY
#					if c[1] == curY:
#						
#					elif c[1] > curY:
#						
#					if (c[1] > curY and c[0] >= curX) or (c[1] >= curY and c[0] > curX):
#						neaY = c[1]
#						neaX 
			elif i == 1: # E
				curX = c[0] if c[0] > curX
		for c in RCrds:
			if i == 2: # S
				curY = c[1] if c[1] < curY
			elif i == 3: # W
				curX = c[0] if c[0] < curX
		Nrst += [[neaX,neaY]]
	return Nrst

def WriteStreetPages(Crds):
	for c in Crds:
		# Cardinal order is always supposed as N, E, S, W
		Nrst = FindNrst(c)
		i = os.listdir('Input/{}'.format(c))
		i.sort()

def Main():
	Crds = GetInputCrds()
	print(Crds)
	

if __name__ == '__main__':
	Main()
